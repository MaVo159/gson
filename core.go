package gson

//import "os"
//import "reflect"
import "io"

//import "bytes"
import "unicode"
import "unicode/utf8"

//import "fmt"

const (
	End Typ = -1
	Non Typ = iota
	Uns
	Idf
	Str
	BadStr
	Num
	BadNum
	Obj
	Lst
	Log
)

type Parser struct {
	byt, lin, col int64
	reader        io.RuneReader
	run           rune
	err           error
	stack         []Node
}

func (p *Parser) Reset() {
	p.byt = 0
	p.lin = 0
	p.col = 0
	p.run = 0
	p.err = nil
	p.stack = p.stack[0:0]
}

func (p *Parser) ReaderError() error {
	return p.err
}

func (p *Parser) Position() (bytes, lines, columns int64) {
	return p.byt, p.lin, p.col
}

func NewParser(r io.RuneReader) *Parser {
	p := &Parser{reader: r}
	return p
}

type Typ int

func (t Typ) String() string {
	switch t {
	case BadNum:
		return "BadNumber"
	case BadStr:
		return "BadString"
	case Non:
		return "None"
	case Uns:
		return "Unspecific"
	case Idf:
		return "Identifier"
	case Str:
		return "String"
	case Num:
		return "Numeric"
	case Obj:
		return "Object"
	case Lst:
		return "List"
	case Log:
		return "Logic"
	default:
		return "Unknown"
	}
}

type ErrorCode int

type Node struct {
	Typ        Typ
	Start, End int64
	Subnodes   int64
}

func (p *Parser) next() {
	r, n, e := p.reader.ReadRune()
	if e != nil {
		p.err = e
	}
	p.run = r
	p.byt += int64(n)
	p.col++
	if r == '\n' {
		p.lin++
		p.col = 0
	}
}

func (p *Parser) skipWhitespace() {
	for unicode.IsSpace(p.run) {
		p.next()
	}
}

func (p *Parser) tryDigestColon() bool {
	p.skipWhitespace()
	if p.run == ':' {
		p.next()
		return true
	}
	return false
}

func (p *Parser) tryDigestDelimiter() bool {
	for unicode.IsSpace(p.run) {
		if p.run == '\n' {
			return true
		}
		p.next()
	}
	switch p.run {
	case '}':
		return true
	case ',':
		p.next()
		return true
	default:
		return false
	}
}

func (p *Parser) tryDigestSequence(seq []rune) bool {
	for i := 0; i < len(seq); i++ {
		if p.run != seq[i] {
			return false
		}
		p.next()
	}
	return !(unicode.IsLetter(p.run) || unicode.IsDigit(p.run))
}

//Return type of digested token. Also return the interpreted length (in bytes) if it is a string or -1.
func (p *Parser) tryDigest() (Typ, int64) {
	//Try keywords or identifiers
	switch {
	case '{' == p.run:
		p.next()
		return Uns, -1
	case '}' == p.run:
		p.next()
		return End, -1
	case '`' == p.run:
		p.next()
		length := int64(0)
		for {
			if p.run == '`' {
				p.next()
				return Str, length
			}
			l := utf8.RuneLen(p.run)
			if p.err != nil || l == -1 {
				return BadStr, -1
			}
			if p.run != '\r' {
				length += int64(l)
			}
			p.next()
		}
	case 't' == p.run:
		p.next()
		if p.tryDigestSequence([]rune{'r', 'u', 'e'}) {
			return Log, -1
		}
	case 'f' == p.run:
		p.next()
		if p.tryDigestSequence([]rune{'a', 'l', 's', 'e'}) {
			return Log, -1
		}
	case unicode.IsLetter(p.run):
		p.next()
	default:
		//Nothing was digested
		//First character is not a letter, nor a '{', try parsing a number
		number := false
		if unicode.IsDigit(p.run) {
			number = true
			for {
				p.next()
				if !unicode.IsDigit(p.run) {
					break
				}
			}
		}
		if p.run == '.' {
			p.next()
			if unicode.IsDigit(p.run) {
				number = true
				for {
					p.next()
					if !unicode.IsDigit(p.run) {
						break
					}
				}
			} else if !number {
				return BadNum, -1
			}
		}
		if number {
			if p.run == 'e' || p.run == 'E' {
				p.next()
				if p.run == '+' || p.run == '-' {
					p.next()
				}
				if unicode.IsDigit(p.run) {
					for {
						p.next()
						if !unicode.IsDigit(p.run) {
							break
						}
					}
					return Num, -1
				} else {
					return BadNum, -1
				}
			}
			return Num, -1
		}
		//Nothing was digested
		{
			return Non, -1
		}
	}
	//Some letters were digested, must be an identifier, digest numbers and digits
	for unicode.IsLetter(p.run) || unicode.IsDigit(p.run) {
		p.next()
	}
	return Idf, -1
}

//Adds a surrounding context of specified type.
//This is usefull if only the content of an object or list should be parsed.
//
//Allowed types are
//List (gson.Lst), Object (gson.Obj) and Unspecified (gson.Uns). Unspecified
//contexts will automatically become lists if their first element is a Value and objects if
//their first element is a Field.
//The Parser will return the node for this context together with the normal EndOfFile error code, if
//the end of the input stream is reached at a position where the "}" character could legally
//end the context.
//
//If the parser encounters a "}" character ending the context before the end of the input stream
//a UnexpectedEndOfListOrObject error code is returned.
//
//This method can only be called before the Parse() method is called for the first time
//after creation of the parser or calling Reset().
func (p *Parser) InitialContext(typ Typ) {
	if p.byt != 0 {
		panic("Called InitialContext after calling Parse() for the first time!")
	}
	switch typ {
	case Lst, Obj, Uns:
		p.stack = append(p.stack, Node{Typ: typ, Start: -1, End: -1})
	}
}

func (p *Parser) Parse() (Node, ErrorCode) {
	if p.byt == 0 {
		p.next()
	}
	var parent *Node
	if len(p.stack) != 0 {
		parent = &p.stack[len(p.stack)-1]
	}

	var node Node
	p.skipWhitespace()
	node.Start = p.byt - 1
	node.Typ, node.Subnodes = p.tryDigest()
	node.End = p.byt - 1

	switch node.Typ {
	case Idf:
		if parent == nil || parent.Typ == Lst || parent.Typ == Idf {
			return node, UnexpectedIdentifier
		}
		if !p.tryDigestColon() {
			return node, MissingColon
		}
		if parent.Typ == Uns {
			parent.Typ = Obj
		}
		parent.Subnodes++
		p.stack = append(p.stack, node)
		return node, NoError
	case BadNum:
		return node, BadNumber
	case BadStr:
		return node, BadString
	case Non:
		if p.err == nil {
			return node, UnexpectedCharacter
		} else if p.err == io.EOF {
			if parent == nil {
				return node, EndOfFile
			} else if p.stack[len(p.stack)-1].Start < 0 {
				node = p.stack[len(p.stack)-1]
				p.stack = p.stack[0:0]
				return node, EndOfFile
			} else {
				return node, UnexpectedEndOfFile
			}
		} else {
			return node, ReaderError
		}

	case End:
		if parent == nil || parent.Typ == Idf || p.stack[len(p.stack)-1].Start < 0 {
			return node, UnexpectedEndOfListOrObject
		}
		parent.End = node.End
		node = *parent
		p.stack = p.stack[0 : len(p.stack)-1]
		parent = nil
		if len(p.stack) == 0 {
			return node, EndOfValue
		} else if p.tryDigestDelimiter() {
			return node, NoError
		} else if p.stack[len(p.stack)-1].Start < 0 && p.err == io.EOF {
			return node, NoError
		} else {
			return node, MissingDelimiter
		}

	default:
		if parent != nil && parent.Typ == Obj {
			return node, UnexpectedValue
		}
		if node.Typ == Uns {
			node.Subnodes = 0
			p.stack = append(p.stack, node)
			if parent != nil {
				parent = &p.stack[len(p.stack)-2]
			}
		} else if parent == nil {
			return node, EndOfValue
		} else if !p.tryDigestDelimiter() {
			return node, MissingDelimiter
		}
		if parent != nil {
			parent.Subnodes++
			if parent.Typ == Uns {
				parent.Typ = Lst
			} else if parent.Typ == Idf {
				p.stack = p.stack[0 : len(p.stack)-1]
				parent = nil
			}
		}
		return node, NoError
	}
}

const EndOfValue ErrorCode = -1
const (
	NoError ErrorCode = iota
	EndOfFile
	UnexpectedIdentifier
	MissingColon
	BadNumber
	BadString
	UnexpectedCharacter
	UnexpectedEndOfFile
	ReaderError
	UnexpectedValue
	MissingDelimiter
	UnexpectedEndOfListOrObject
)
